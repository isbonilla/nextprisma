
import { NextRequest, NextResponse } from 'next/server'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export async function GET(req:NextRequest, res:NextResponse) {
    const allCourse = await prisma.course.findMany()
    console.log(allCourse)
    
    return NextResponse.json({
      message: 'Extracion de Course exitosa',
      data: allCourse
    })
}
